def selection(array)
    for i in 0...(array.length-1)
        p "first loop"
        min_val = i
        for j in (i+1)...array.length
            p "second loop"
            if array[j] < array[min_val]
                min_val = j
            end
            if min_val !=i
                array[min_val], array[i] = array[i], array[min_val]
            end
        end
    end
    return array
end
p selection([3,2,4,1])