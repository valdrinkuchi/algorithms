def binary_search(sequence, item)
    begin_index = 0
    end_index = sequence.length() -1
    while begin_index < end_index
        midpoint = begin_index + ((end_index - begin_index)/2).to_i
        midpoint_value = sequence[midpoint]
        if midpoint_value == item
            return midpoint
        elsif item < midpoint_value
            end_index = midpoint -1 
        else
           begin_index = midpoint + 1     
        end
    end
end

seq_a = [2,4,5,7,8,9,11,15,18]
item_q = 11

puts binary_search(seq_a,item_q)