def quick_sort(array)
    length = array.length
    if length < 1
        return array
    else
        pivot = array.pop
    end
    items_greater = []
    items_lower = []

    array.each do |item|
        if item > pivot
            items_greater << item
        else
            items_lower << item    
        end
    end
    return quick_sort(items_greater) + [pivot] + quick_sort(items_lower)
end

p quick_sort([3,2,4,1])