def insertion_sort(array)
    for i in 1...(array.length)
        value_sorted = array[i]
        while array[i-1] > value_sorted && i > 0
            array[i-1], array[i] = array[i], array[i-1]
            i = i - 1
        end
        
    end
    return array
end
puts insertion_sort([3,1,4,2,7])