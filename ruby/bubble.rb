def bubble(list_a)
    indexing = list_a.length() - 1
    sorted = false
    while not sorted do
        sorted = true
        indexing.times do |i|
            if list_a[i] > list_a[i+1]
                sorted = false
                list_a[i], list_a[i+1] = list_a[i+1], list_a[i]
            end
        end
        return list_a
    end
end

puts bubble([1,3,5,4,2])